---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

My name is Ian. I have the following qualities:

- I have rocked a great beard in the past
- I'm extremely loyal
- I like cycling

Me and Linux is picky about our friends...

### my history

I've done a lot of different things in life, but software has always been my profession.
